

export default class AppConstants {
    static readonly JETSON_KITCHEN_ID:string='jetson-kitchen-id'
    static readonly JETSON_KITCHEN:string='jetson-kitchen'
    static readonly JETSON_KITCHEN_NAME:string='jetson-kitchen-name'
    static readonly JETSON_KITCHEN_CITY:string='jetson-kitchen-city'
    static readonly AUTH_TOKEN:string='user-token'
}