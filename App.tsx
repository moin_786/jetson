import { NavigationContainer } from '@react-navigation/native';
// import {persistor, store} from '@redux/store';
import { persistor, store } from './src/shared/redux/store';
import { navigationRef } from './src/shared/services/nav.service';
import React, { useEffect } from 'react';
import 'react-native-gesture-handler';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import Toast from 'react-native-toast-message';
import { Provider, } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import Routes from './src/routes';
import SplashScreen from 'react-native-splash-screen';
import 'react-native-gesture-handler';
import { I18nManager, LogBox } from 'react-native';
import client, { hubClient } from './src/shared/config/apolloClient';
import { ApolloProvider } from '@apollo/client';
import { EXPO_PUBLIC_KITCHEN_GRAPHQL } from '@env';



const App = () => {
  useEffect(() => {
    SplashScreen.hide();

  }, []);
console.log("EXPO_PUBLIC_KITCHEN_GRAPHQLEXPO_PUBLIC_KITCHEN_GRAPHQL",EXPO_PUBLIC_KITCHEN_GRAPHQL)
  return (
    <>
        <Provider store={store}>
          <PersistGate persistor={persistor}>
            <SafeAreaProvider>
              <NavigationContainer ref={navigationRef} >
                <Routes />
              </NavigationContainer>
            </SafeAreaProvider>
          </PersistGate>
        </Provider>
    </>
  );
};

export default App;
