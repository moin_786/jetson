import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions,
  StyleSheet,
} from 'react-native';
import { gql, useQuery, useSubscription } from '@apollo/client';

import React, {useState} from 'react';
import DropDownPicker from 'react-native-dropdown-picker';
import { useDispatch, useSelector } from 'react-redux';
import { setUser } from '../../shared/redux/reducers/userReducer';
import SoundPlayer from 'react-native-sound-player'
import Sound from 'react-native-sound';

const ORDER_SUBSCRIPTION = gql`
  subscription orderByKitchenSubscription($kitchenId: String!) {
    orderByKitchenSubscription(kitchenId: $kitchenId,eventType:"NEW_ORDER_RECEIVED") {
      eventType
      payload {
        id
      }
    }
  }
`;

export default function Kitchen() {

  const dispatch = useDispatch()
  const {cities, kitchens, selectedCity, status, selectedKitchen} = useSelector(
    (state: any) => state.root?.kitchen,
  );
 
  Dimensions.get("screen").width


// console.log("selectedKitchenselectedKitchen",selectedKitchen?.id)
//   let {loading,error}=useSubscription(ORDER_SUBSCRIPTION,{
//     variables:{
//       kitchenId: selectedKitchen?.id
//     },
//     onData:()=>{
//       PlaySound();
      
//     },
//     onError:(err)=>{
//       console.log(err)
//     },
//   })

const PlaySound  = async () =>{

 let file =  require('../../assets/alert.mp3')

  let  soundplay =await new Sound(file, (error) => {
    if (error) {
      console.log('failed to load the sound', error);
      return;
    }
  
    soundplay.play((success) => {
      if (success) {
        console.log('successfully finished playing');
      } else {
        console.log('playback failed due to audio decoding errors');
      }
    });
  });
}
  return (
    <View style={style.main}>
    <StatusBar barStyle='dark-content' backgroundColor='#000000' ></StatusBar>
    <View style={style.innerWrapper}>
        
      {selectedKitchen && selectedKitchen.name && <Text style={{color:'#000000',fontSize:25,fontWeight:'500', }}>{selectedKitchen.name}</Text>}
      <TouchableOpacity 
        style={style.btn}
        onPress={()=>{
          // PlaySound() Test sound
        }}>
              <Text style={style.btntxt}>Reset Kitchen</Text>
      </TouchableOpacity>
    </View>
     
  </View>
  );
}

const style = StyleSheet.create({
  main: {flex:1,display:'flex',alignItems:'center',justifyContent:'center', backgroundColor:'#000000'},
  btntxt: {color: '#FFFFFF', fontSize: 20, fontWeight: '500'},
  btn: {display:'flex',alignItems:'center',justifyContent:'center',width:350,height:50,backgroundColor:'#1B6247',borderRadius:8,marginTop:30},
  innerWrapper: {display:'flex',width:540,backgroundColor:'#FFFFFF',alignItems:'center',justifyContent:'center',padding:60,height:200},


});