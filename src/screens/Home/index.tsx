import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions,
  StyleSheet,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import DropDownPicker from 'react-native-dropdown-picker';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import {ROUTES} from '../../shared/utils/routes';
import {
  Status,
  getAllKitchenThunkAsync,
  setSelectedCity,
  setSelectedKitchen,
} from '../../shared/redux/reducers/kitchenSlice';
import {useDispatch, useSelector} from 'react-redux';

export default function Home() {
  const dispatch = useDispatch();
  const focus = useIsFocused();
  const navigation = useNavigation();
  const [open, setOpen] = useState(false);
  const [open1, setOpen1] = useState(false);
  const [value, setValue] = useState<any>(null);
  const [value1, setValue1] = useState<any>(null);
  const [items1, setItems1] = useState([]);
  const [items, setItems] = useState([]);
  Dimensions.get('screen').width;
  const {cities, kitchens, selectedCity, status, selectedKitchen} = useSelector(
    (state: any) => state.root?.kitchen,
  );

  useEffect(() => {
    if (selectedKitchen) {
      navigation.replace(ROUTES.KITCHEN);
    }
    if (cities) {
      setItems(cities);
    }
    if (kitchens) {
      setItems1(kitchens);
    }

    if (status == Status.none) {
      dispatch(getAllKitchenThunkAsync());
    }
  }, [cities, kitchens, focus, selectedKitchen]);
  
  const onPress = () => {
    let kitchen = kitchens.filter((val: any) => val.id == value1);
    let city = cities.filter((val: any) => val.name == value);

    if (kitchen.length > 0) {
      dispatch(setSelectedKitchen(kitchen[0]));
    }

    if (city.length > 0) {
      dispatch(setSelectedCity(city[0]));
    }
  };
  return (
    <View style={style.main}>
      <StatusBar barStyle="dark-content" backgroundColor="#000000"></StatusBar>
      <View style={style.innerWrapper}>
        <View style={style.DropDownPicker}>
          <DropDownPicker
            style={style.drop}
            placeholder="Select city"
            schema={{
              label: 'name',
              value: 'name',
            }}
            open={open}
            value={value}
            items={items}
            setOpen={setOpen}
            setValue={setValue}
            setItems={setItems}
          />
        </View>
        {value && (
          <View style={style.DropDownPicker}>
            <DropDownPicker
              placeholder="Select Kitchen"
              style={style.drop}
              open={open1}
              value={value1}
              schema={{
                label: 'name',
                value: 'id',
              }}
              items={items1}
              setOpen={setOpen1}
              setValue={setValue1}
              setItems={setItems1}
            />
          </View>
        )}
        <TouchableOpacity style={style.btn} onPress={onPress}>
          <Text style={style.btntxt}>Select Kitchen</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const style = StyleSheet.create({
  main: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000000',
  },
  btntxt: {color: '#FFFFFF', fontSize: 20, fontWeight: '500'},
  btn: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: 350,
    height: 50,
    backgroundColor: '#1B6247',
    borderRadius: 8,
  },
  innerWrapper: {
    display: 'flex',
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 60,
  },
  DropDownPicker: {
    width: '100%',
    height: 50,
    zIndex: 1000,
    marginBottom: 30,
    paddingHorizontal: 15,
  },
  drop: {
    borderColor: '#868E96',
    height: 50,
    backgroundColor: '#F7FAF7',
  },
});
