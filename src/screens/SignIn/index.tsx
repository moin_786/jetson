import React, { useEffect, useRef, useState } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Platform,
  TextInput,
  Alert,
  Dimensions,
  ScrollView,
  Image,
  KeyboardAvoidingView,
  ActivityIndicator,
  StyleSheet,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import JetsonLogoSVG from '../../assets/jetsonSVGlogo';
import { setUser, signInAsyncThunk } from '../../shared/redux/reducers/userReducer';
import { Formik, useFormik } from 'formik';
import { LoginSchema } from '../../shared/validation';




const SignIn = ({ navigation }: any) => {
  const { loading, error } = useSelector((state: any) => state.root.user)
  const dispatch = useDispatch()

  const formik = useFormik({
    validationSchema: LoginSchema,
    initialValues: {
      username: '',
      password: '',
    },
    onSubmit: async (values: any) => {
      dispatch(signInAsyncThunk({ username: values.username, password: values.password }));
    },
  });

  return (
    <View style={style.main}>
      <KeyboardAvoidingView keyboardVerticalOffset={Platform.OS === 'ios' ? 200 : -150}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={style.innerWrapper} >
        <JetsonLogoSVG></JetsonLogoSVG>
        <Text style={style.title}>Sign In</Text>

        <TextInput
          value={formik.values.username}
          onChangeText={formik.handleChange('username')}


          keyboardType='email-address' style={[style.inputStyle, formik.errors.username && { borderColor: 'red', borderWidth: 1 }]}
          placeholder='email'
          placeholderTextColor={"#818181"}

        />

        <TextInput style={[style.inputStyle, formik.errors.password && { borderColor: 'red', borderWidth: 1 }]}
          value={formik.values.password}
          onChangeText={formik.handleChange('password')}
          secureTextEntry={true} placeholder='password' placeholderTextColor={"#818181"} />


        <TouchableOpacity
          style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', width: "100%", height: 50, backgroundColor: '#1B6247', borderRadius: 8 }}
          onPress={() => {
            formik.handleSubmit();
          }}>
          {loading ? <ActivityIndicator size="large" color="#FFFFFF" ></ActivityIndicator> : <Text style={style.btnText}>Sign In</Text>}

        </TouchableOpacity>

        {!!error && <View style={style.err}>
          <Text style={{ color: '#FFFFFF' }}>{error}</Text>
        </View>}

      </KeyboardAvoidingView>


    </View>
  );
};

const style = StyleSheet.create({
  main: {
    flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'center', paddingHorizontal: 10
  },
  innerWrapper: { width: "100%", display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' },
  title: { fontSize: 32, fontWeight: '500', marginTop: 30, marginBottom: 50, color: "black" },
  inputStyle: { width: '100%', height: 50, backgroundColor: '#F7FAF7', marginBottom: 30, fontSize: 16, fontWeight: '400', paddingLeft: 23, color: '#000000', borderRadius: 8 },
  btnText: { color: '#FFFFFF', fontSize: 20, fontWeight: '500' },
  err: { marginTop: 20, width: '100%', padding: 10, backgroundColor: 'red', borderRadius: 4 }
})
export default SignIn;
