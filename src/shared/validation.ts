import * as Yup from 'yup';

export const LoginSchema = Yup.object().shape({
    username: Yup.string().email('Invalid_Email').required('Email_Required'),
    password: Yup.string()
      .required('Password_Required')
      .min(6, 'Password_too_Short'),
  });