
import { ApolloClient, ApolloLink, ApolloProvider, HttpLink, InMemoryCache, concat, from, split } from '@apollo/client';
import { getMainDefinition } from '@apollo/client/utilities';
import { createClient } from 'graphql-ws';
import { GraphQLWsLink } from '@apollo/client/link/subscriptions';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { setContext } from '@apollo/client/link/context';
import AppConstants from '../../../app.constants';
import { onError } from '@apollo/client/link/error';
import { EXPO_PUBLIC_KITCHEN_GRAPHQL,EXPO_PUBLIC_KITCHEN_SUBSCRIPTION, EXPO_PUBLIC_JETSON_HUB_GRAPHQL } from '@env';
import { store } from '../redux/store';
// import { router } from 'expo-router';
const x = store.getState().root.user
console.log("xxxxxxxxxxxxxxxxxxxxxx>>>",x?.token)
let token  =  x?.token
const httpLink = new HttpLink({
    uri:EXPO_PUBLIC_KITCHEN_GRAPHQL
});

// const wsLink = new GraphQLWsLink(createClient({
//     url: 'wss://fulfiller-develop.jetsontech.ca/kitchen/graphql',
// }));

const httpJetsonHubLink = new HttpLink({
    uri:EXPO_PUBLIC_JETSON_HUB_GRAPHQL,
});

let activeSocket:any,
  timedOut:any,
  pingSentAt = 0,
  latency = 0;
  
// 'wss://fulfiller-develop.jetsontech.ca/kitchen/graphql'
const subscriptionClient = createClient({
  url: `${EXPO_PUBLIC_KITCHEN_SUBSCRIPTION}`,
  keepAlive: 10_000, // ping server every 10 seconds
  shouldRetry: () => true,

  on: {
    opened: (socket) => (activeSocket = socket),
    ping: (received) => {
      if (!received /* sent */) {
        pingSentAt = Date.now();
        timedOut = setTimeout(() => {
          console.log("ping failed")
          // if (activeSocket.readyState === WebSocket.OPEN)
          //   activeSocket.close(4408, 'Request Timeout');

          
          
        }, 5_000); // wait 5 seconds for the pong and then close the connection
      }else{
        console.log("pinged")
      }
    },
    pong: (received) => {
      if (received) {
        console.log("ponged")
        latency = Date.now() - pingSentAt;
        clearTimeout(timedOut); // pong is received, clear connection close timeout
      }
    },
  },
});

subscriptionClient.on("closed", () => {
  
});

const wsLink = new GraphQLWsLink(subscriptionClient);

const authLink = setContext(async (_, { headers }) => {
    // let token = await AsyncStorage.getItem(AppConstants.AUTH_TOKEN).catch(
    //   (e) => console.log(e)
    // );

   // console.log(token)
  
    return {
      headers: {
        ...headers,
        Authorization: token ? `Bearer ${token}` : '',
      },
    };
  });
  

const authMiddleware = new ApolloLink((operation, forward) => {
    // add the authorization to the headers
    operation.setContext(async ({ headers = {} }) => {
        // let token = await AsyncStorage.getItem(AppConstants.AUTH_TOKEN).catch(
        //     (e) => console.log(e)
        //   );

          console.log(token)

          console.log("Bearer " +token || null)
   
        return {
            headers: {
                ...headers,
                Authorization: "Bearer " +token,
            },
        }
    });
  
    return forward(operation);
  });

  const logoutLink = onError(({ networkError }) => {
   console.log(networkError)
  })

  const responseLoggingLink = new ApolloLink((operation, forward) => {
    return forward(operation).map(response => {
        console.log(response)
      return response;
    });
  });


// The split function takes three parameters:
//
// * A function that's called for each operation to execute
// * The Link to use for an operation if the function returns a "truthy" value
// * The Link to use for an operation if the function returns a "falsy" value
const splitLink = split(
    ({ query }) => {
        const definition = getMainDefinition(query);
        return (
            definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription'
        );
    },
    wsLink,
    concat(authMiddleware,httpLink),
);

// Initialize Apollo Client
const client = new ApolloClient({
    link: splitLink,
    cache: new InMemoryCache()
});



export const hubClient = new ApolloClient({
    link:authLink.concat(httpJetsonHubLink),
    cache: new InMemoryCache()
});



export default client
