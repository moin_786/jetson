import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { hubClient } from "../../config/apolloClient";
import { ApolloQueryResult, gql } from "@apollo/client";


const getKitchens = gql`
  query kitchens {
    kitchens {
      id
      name
      city
      fulfillers {
        id
        name
      }
    }
    cities{
      name
    }
  }
`;

export const getAllKitchenThunkAsync:any = createAsyncThunk(
    "kitchen/getAllKitchenThunkAsync",
    async () => {
      let result = await hubClient
        .query({
          query: getKitchens,
        })
        .then((result: any) => {
            console.log( JSON.stringify(result.data))
            return result.data;
        });
        console.log("getAllKitchenThunkAsync====>>>>>",result)
      return result;
    }
  );


  const initialState={
        status:'none',
        cities:null,
        kitchens:null,
        selectedKitchen:null,
        selectedCity:null
  }


  export enum Status {
    fulfilled='fulfilled',
    rejected='rejected',
    pending='pending',
    none='none'
  }
  const kitchenSlice = createSlice({
    name:'kitchen',
    initialState,
    reducers:{
      setSelectedKitchen:(state,action)=>{
        state.selectedKitchen = action.payload
      },
      setSelectedCity:(state,action)=>{
        state.selectedCity = action.payload
      },
    
     
    },
    extraReducers:(builder)=>{
        builder.addCase(getAllKitchenThunkAsync.fulfilled,(state,action)=>{
            state.status = Status.fulfilled
            if(action.payload){
              state.kitchens = action.payload.kitchens
              state.cities = action.payload.cities
            }
        }),
        builder.addCase(getAllKitchenThunkAsync.rejected,(state,action)=>{
            state.status = Status.rejected
        }),
        builder.addCase(getAllKitchenThunkAsync.pending,(state,action)=>{
            state.status = Status.pending
        })
    }
  })

  export const {setSelectedKitchen,setSelectedCity} = kitchenSlice.actions

  // export const selectKitchen = (state:RootState)=>state.kitchen;


  export default kitchenSlice.reducer;