import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { NativeModules } from 'react-native';
import { EXPO_PUBLIC_USERPOOL_ID , EXPO_PUBLIC_APP_CLIENT_ID} from '@env';
import { AuthenticationDetails, CognitoUser, CognitoUserPool, CognitoUserSession } from 'amazon-cognito-identity-js';

const userPoolId = EXPO_PUBLIC_USERPOOL_ID;
const clientId = EXPO_PUBLIC_APP_CLIENT_ID;

console.log(userPoolId)


const poolData = {
  UserPoolId: userPoolId,
  ClientId: clientId,
};
const userPool: CognitoUserPool = new CognitoUserPool(poolData);

export const signInAsyncThunk:any = createAsyncThunk(
  'auth/fetchByIdStatus',
  async ({username,password}:{username:string,password:string}, thunkAPI) => {
   const poolData = {
  UserPoolId: userPoolId,
  ClientId: clientId,
};
    const userData = {
      Username: username,
      Pool: userPool,
    };

    const authenticationData = {
      Username: username,
      Password: password,
    };
    const authenticationDetails = new AuthenticationDetails(authenticationData);

    const cognitoUser = new CognitoUser(userData);

   let authResult: CognitoUserSession = await new Promise((resolve, reject) => {

    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function (res: any) {

        resolve(res);
      },
      onFailure: function (err: any) {

        reject(err);
      },
      newPasswordRequired: function (res: any) {
        cognitoUser?.completeNewPasswordChallenge(
          password,
          {},
          {
            onSuccess: (res) => {
              console.log(res);
              resolve(res);
            },
            onFailure: (err) => {
              console.log(err);
              reject(err);
            },
          }
        );
      },
    });
  });

  let token = authResult.getAccessToken().getJwtToken();
  let user = authResult.getIdToken().payload;



    return {
      token,
      user
    };

  }
)



const initialState = {
  user: null,

};

export const userReducer = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state, action) => {
      state.user = action.payload;
    },
 
  },
  extraReducers: (builder) => {
   builder.addCase(signInAsyncThunk.fulfilled, (state:any, action:any) => {
    state.loading = false
    state.token = action.payload.token
      state.user = action.payload.user
    })
   builder.addCase(signInAsyncThunk.pending, (state:any, action:any) => {
    state.loading = true
      state.user = null
      state.token = null
    })
   builder.addCase(signInAsyncThunk.rejected, (state:any, action:any) => {

      state.loading = false
      state.user = null
      state.token = null
      state.error = action?.error?.message||'Unknown error occurred';
    })
  },
});

export const {setUser, } = userReducer.actions;
export const selectAuth = (state: any) => state.user

export default userReducer.reducer;
