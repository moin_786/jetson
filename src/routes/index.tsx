import React, { useEffect, useLayoutEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AuthStack from './stacks/authStack';
import { TouchableOpacity, View, Image, Keyboard, Text } from 'react-native';
import MainStack from './stacks/mainStack/MainNavigator';




const Routes = () => {

  const dispatch = useDispatch();

const {user} = useSelector((state:any)=>state.root.user)
  return (<View style={{flex:1}}>
{user? <MainStack /> : <AuthStack />}
  </View>
   
          
     
  );
};




export default Routes;
