import React, { useEffect, useState } from 'react';
import { TouchableOpacity, View, Image, Keyboard, Text } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import styles from './styles';




import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { ROUTES } from '../../../shared/utils/routes';



import Home from '../../../screens/Home';
import Kitchen from '../../../screens/Home/kitchenScreen';
import { useSelector } from 'react-redux';
import { ApolloProvider } from '@apollo/client';
import client from '../../../shared/config/apolloClient';

const Stack = createStackNavigator();





export default function MainStack() {
  const {cities, kitchens, selectedCity, status, selectedKitchen} = useSelector(
    (state: any) => state.root?.kitchen,
  );

  return (
    <>
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        ...TransitionPresets.SlideFromRightIOS,
      }}
    >
    {!selectedKitchen&&  <Stack.Screen name={ROUTES.HOMESCREEN} component={Home} />}
      <Stack.Screen name={ROUTES.KITCHEN} component={Kitchen} />
    
    </Stack.Navigator>
    {/* </ApolloProvider> */}
  </>
  );
}
