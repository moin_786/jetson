import { Platform, StyleSheet } from 'react-native';
import { moderateScale } from '@theme/metrics';
import { COLORS } from '@theme/colors';
import { FONTS } from '@theme/fonts';

const styles = StyleSheet.create({
  tabbarContainer: {
    flexDirection: 'row',
    backgroundColor: '#FFF',
    paddingTop: moderateScale(5),
    height: moderateScale(65),
    position: 'relative',
    paddingHorizontal: moderateScale(5),
  },
  tabItemContainer: {
    flex: 1,
    alignItems: 'center',
    height: moderateScale(50),
    paddingTop: moderateScale(5),
    justifyContent: 'space-evenly',
  },
  tabItemText: {
    // color:'red'
    fontSize: moderateScale(11),
    lineHeight: moderateScale(18),
    fontFamily: FONTS.RubikRegular,

    // paddingTop: moderateScale(4),
  },
  headerContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'red',
    alignItems: 'center',
  },
  headerImageContainer: {
    alignItems: 'center',
  },
  profileImage: {
    borderRadius: moderateScale(16),
    width: moderateScale(32),
    height: moderateScale(32),
    borderWidth: moderateScale(1),
    borderColor: 'yellow',
  },
  dropShadow: {
    // shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,

    elevation: 24,
  },
  QRBtn: {
    position: 'absolute',
    height: 80,
    width: 80,
    borderRadius: 50,
    borderWidth: 10,
    borderColor: '#26272A',
    bottom: moderateScale(80),
    zIndex: 9999,
    left: '40%',
  },
  TabsName: {
    fontSize: moderateScale(12),
    fontFamily: FONTS.RubikMedium,
  },
  QRBtnMap: {
    position: 'absolute',
    height: 80,
    width: 80,
    borderRadius: 50,
    borderWidth: 10,
    borderBottomColor: '#26272A',
    borderLeftColor: '#26272A',
    borderRightColor: '#26272A',
    borderTopColor: '#26272A',
    bottom: moderateScale(80),
    zIndex: 9999,
    left: '40%',
  },
  tabBarIconImage: { height: moderateScale(29), width: moderateScale(29) },
});
export default styles;
