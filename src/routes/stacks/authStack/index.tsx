import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';

import SignIn from '../../../screens/SignIn/index';
import {ROUTES} from '../../../shared/utils/routes';
import React from 'react';

const Stack = createStackNavigator();

const AuthStack = () => {
  return (
    <>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
          ...TransitionPresets.SlideFromRightIOS,
        }}
      >
        <Stack.Screen name={ROUTES.SIGNIN} component={SignIn} />
      
      </Stack.Navigator>
    </>
  );
};

export default AuthStack;
