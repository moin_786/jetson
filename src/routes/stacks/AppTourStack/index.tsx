import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import AppTour from '@screens/appTour';
import DeleteAccount from '@screens/auth/DeleteAccount';
import Enrollment from '@screens/auth/Enrollment';
import ResetByEmail from '@screens/auth/ResetByEmail';
import ResetByOTP from '@screens/auth/ResetByOTP';
import ResetPassword from '@screens/auth/ResetPassword';
import SignIn from '@screens/auth/SignIn';
import Signup from '@screens/auth/SignUp';
import Signupmain from '@screens/auth/SignUp/Signupmain';
import { ROUTES } from '@utils/routes';
import React from 'react';

const Stack = createStackNavigator();

const AppTourStack = () => {
  return (
    <>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
          ...TransitionPresets.SlideFromRightIOS,
        }}
      >
        <Stack.Screen
          name={ROUTES.APPTOUR} component={AppTour} />
      </Stack.Navigator>
    </>
  );
};

export default AppTourStack;
